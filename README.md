Aвто фарм
=========

Небольшой пример работы программы

[Видео на YuoTube](https://www.youtube.com/watch?v=7Ug1jZZv1PM)


Макрос программы UOpilot для автоматического фарма в игре Archeage

    // авто фарм Archeage
    set #x1 770
    set #y1 350
    set #x #x1
    set #y #y1
    set #turn 57
    set #turn_right #x1 + #turn
    set #turn_left #x1 - #turn
    set #flag 1
    
    set #color1 2833556  // цвет начала полоски хп
    set #color2 3228839 // конец полоски хп
    set #color3 3492792 // альтернативное начало полоски хп
    set #x1_hp 790
    set #y1_hp 0
    set #x2_hp 1240
    set #y2_hp 80
    set #b 0
    
    // Блок кода для вспомогательных скиллов
    wait random(50)+50
    move 1000, 1003       // переместить указатель на умение
    wait random(50) + 50  //задержка
    kleft 1000, 1003      // нажать умение
    wait random(50) + 50  // задержка
    move 947, 999
    wait random(50) + 50
    kleft 947, 999
    wait random(50) + 50
    sendex {f}            // для подбора дропа вокруг персонажа
    // конец блока
    
    sendex {1}
    wait 200
    set #b findcolor (#x1_hp #y1_hp #x2_hp #y2_hp 1 1 #color2 %arr1 2 abs)
    if #b = 0
         move #x1 #y1
         wait random(50) + 50
         kright_down #x1, #y1
         wait random(50) + 50
         set #x #x1
    end_if
    while #b = 0
        sendex {1}
        set #b findcolor (#x1_hp #y1_hp #x2_hp #y2_hp 1 1 #color2 %arr1 2 abs)
        if #x >= #turn_right and #flag = 1
            set #flag -1
        end_if
        if #x <= #turn_left and #flag = -1
            set #flag 1
        end_if
        set #x #x + #flag
        move #x #y
    end_while
    kright_up #x #y
    wait random(50) + 50
    
    set #a findcolor (#x1_hp #y1_hp #x2_hp #y2_hp 1 1 #color1 %arr2 2 abs)
    if #a = 0
        set #a findcolor (#x1_hp #y1_hp #x2_hp #y2_hp 1 1 #color3 %arr2 2 abs)
        set #color1 #color3
    end_if
    while %arr2[1 1], %arr2[1 2] #color1
        // блок кода для восстановления маны
        if_not 221, 56  14057003
            if timer > 60000
                set timer
                wait 5s
                sendex e
                wait 1300
                sendex r
                wait 1300
            end_if
        end_if
        // конец блока
        sendex {1}
        wait random(50) + 50
    end_while